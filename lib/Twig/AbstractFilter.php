<?php namespace Anvil\Twig;

use Anvil\Twig\AbstractTwigFunction;
use Anvil\Twig\FilterStore;
use Dorigo\Singleton\Singleton;

abstract class AbstractFilter extends AbstractTwigFunction {

    protected function getStore() : void {
        $this->store = FilterStore::getInstance();
    }

}