<?php namespace Anvil\Twig;

use Dorigo\Singleton\Singleton;

abstract class AbstractTwigFunction extends Singleton {

    protected $title;
    protected $name;
    protected $store;

    protected $defaultFunction = true;
    protected $functions = [];

    final protected function __construct() {
        $this->getStore();
        $this->getName();
        $this->load();

        if($this->defaultFunction) {

            $this->set($this->name, [$this, 'callback']);

        } else if(!empty($this->functions)) {
            foreach($this->functions as $name => $callback) {
                if(!is_callable($callback)) {

                    throw new \Error("The callback `{$name}` in `{$this->name}` does not exist.");

                }

                $this->set($name, $callback);

            }
        } else {
            $this->set($this->name);
        }
    }

    abstract protected function getStore() : void;

    final public function addFunction($name, $callback) {

        $this->defaultFunction = false;
        $this->functions[$name] = $callback;

    }

    public function load() {

    }

    protected function set(string $name, $callback = null) {

        if($callback && is_callable($callback)) {
            $this->store->set($name, $callback);
        } else {
            $this->store->set($name, $this);
        }

    }

    final public static function register() : self {
        return self::getInstance();
    }

    final protected function getName() {

        $class = substr(strrchr(get_class($this), '\\'), 1);
        $name = preg_replace('/([a-z0-9])([A-Z])/', '\1_\2', $class);

        $this->name = strtolower($name);
    }

}