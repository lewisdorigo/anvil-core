<?php namespace Anvil\Twig;

use Anvil\Twig\AbstractTwigFunction;
use Anvil\Twig\FunctionStore;
use Dorigo\Singleton\Singleton;

abstract class AbstractFunction extends AbstractTwigFunction {

    protected function getStore() : void {
        $this->store = FunctionStore::getInstance();
    }

}