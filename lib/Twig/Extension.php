<?php namespace Anvil\Twig;

use Dorigo\Singleton\Singleton;
use Anvil\Twig\{FunctionStore,FilterStore,ContextStore};
use Timber\Twig_Function as TwigFunction;
use Timber\Twig_Filter as TwigFilter;

class Extension extends Singleton {

    private $functionStore;
    private $filterStore;
    private $variableStore;

    protected function __construct() {
        $this->functionStore = FunctionStore::getInstance();
        $this->filterStore   = FilterStore::getInstance();
        $this->contextStore = ContextStore::getInstance();

        add_filter("timber/twig", [$this, 'loadFunctions'], 0, 1);
        add_filter("timber/twig", [$this, 'loadFilters'], 0, 1);
        add_filter("timber/context", [$this, 'loadContext'], 0, 1);
    }

    public function loadFunctions(\Twig\Environment $twig) {

        foreach($this->functionStore as $name => $function) {

            $twig->addFunction(new TwigFunction($name, $function));
        }

        return $twig;

    }

    public function loadFilters(\Twig\Environment $twig) {

        foreach($this->filterStore as $name => $filter) {
            $twig->addFilter(new TwigFilter($name, $filter));
        }

        return $twig;

    }

    public function loadContext(array $context) {

        foreach($this->contextStore as $name => $variable) {
            $context[$name] = $variable;
        }

        return $context;

    }

}