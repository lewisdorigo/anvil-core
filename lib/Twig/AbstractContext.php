<?php namespace Anvil\Twig;

use Anvil\Twig\AbstractTwigFunction;
use Anvil\Twig\ContextStore;
use Dorigo\Singleton\Singleton;

abstract class AbstractContext extends AbstractTwigFunction {
    protected $callbacks = null;

    protected function getStore() : void {
        $this->store = ContextStore::getInstance();
    }

}