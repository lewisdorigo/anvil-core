<?php namespace Anvil\Store;

abstract class AbstractStore extends \ArrayIterator {
    
    public function get(string $key) {
        
        return $this->offsetExists($key) ? $this->offsetGet($key) : null;
        
    }
    
    public function set(string $key, $value) {
        
        $this->offsetSet($key, $value);
        
    }
    
    public function setAll(iterable $values) {
        
        foreach($values as $key => $value) {
            
            $this->offsetSet($key, $value);
            
        }
        
    }
    
    public function exists($key) {
        return $this->offsetExists($key);
    }
    
    public function add($value) {
        $this->append($value);
    }
    
    public function unset(string $key) {
        if($this->offsetExists($key)) {
            $this->offsetUnset($key);
            return true;
        }
        
        return false;
    }
    
}