<?php namespace Anvil\Components\Fields;

class TextField extends AbstractField {
    
    protected function type() : string {
        return 'text';
    }
    
    protected function defaultSettings() : array {
        return [
            'default_value' => '',
            'placeholder' => '',
            'prepend' => '',
            'append' => '',
            'maxlength' => ''
        ];
    }
    
}