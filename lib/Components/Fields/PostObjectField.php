<?php namespace Anvil\Components\Fields;

class PostObjectField extends AbstractField {

    protected function type() : string {
        return 'post_object';
    }

    protected function defaultSettings() : array {
        return [
            'post_type' => null,
            'taxonomy' => null,
            'allow_null' => 1,
            'multiple' => 0,
            'return_format' => 'object',
            'ui' => false,
        ];
    }

}