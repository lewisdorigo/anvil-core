<?php namespace Anvil\Components\Fields;

class TaxonomyField extends AbstractField {

    protected static $allowedFieldTypes = [
        'checkbox',
        'multi_select',
        'radio',
        'select'
    ];

    protected function type() : string {
        return 'taxonomy';
    }

    public function setTaxonomy(string $taxonomy) : void {
        $this->offsetSet('taxonomy', $taxonomy);
    }

    public function setFieldType(string $type) : void {

        if(!in_array($type, self::$allowedFieldTypes)) {
            trigger_error('Allowed field types are '.join(', ', self::$allowedFieldTypes));
            return;
        }

        $this->offsetSet('field_type', $taxonomy);
    }

    protected function defaultSettings() : array {
        return [
			'taxonomy' => null,
			'field_type' => 'select',
			'add_term' => 0,
			'save_terms' => 0,
			'load_terms' => 0,
			'return_format' => 'id',
			'multiple' => 0,
			'allow_null' => 0,
        ];
    }

}