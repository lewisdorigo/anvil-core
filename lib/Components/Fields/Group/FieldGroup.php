<?php namespace Anvil\Components\Fields\Group;

use Anvil\Components\Fields\Group\FieldGroupStore;

class FieldGroup extends \ArrayIterator {

    private $store;

    protected static $defaultObject = [
        'location' => [],
        'menu_order' => 0,
        'position' => 'normal',
        'style' => 'default',
        'label_placement' => 'top',
        'instruction_placement' => 'label',
        'hide_on_screen' => '',
        'active' => true,
        'description' => ''
    ];

    public function __construct(string $name, array $fields, array $settings = []) {


        $this->store = FieldGroupStore::getInstance();

        $this->setName($name);
        $this->setKey($name);
        $this->addFields(...$fields);

        $this->combineSettings($settings);

        $this->store->offsetSet($this->offsetGet('key'), $this);

    }

    final private function setName(string $name) : void {
        $this->offsetSet('title', $name);
    }

    final private function setKey(string $name) : void {
        if($this->offsetExists('key')) {
            return;
        }

        $i = 0;

        $name = sanitize_title($name);
        $key = "group_anvil_{$name}";

        while($this->store->exists($key)) {
            $i++;
            $key = "{$key}-{$i}";
        }

        $this->offsetSet('key', $key);
    }

    final protected function addFields(...$fields) {

        $currentFields = $this->offsetExists('fields') ? $this->offsetGet('fields') : [];

        foreach($fields as $field) {
            $currentFields[] = (array) $field;
        }

        $this->offsetSet('fields', $currentFields);

    }

    final public function combineSettings(array $settings) : void {
        $settings = array_merge(self::$defaultObject, $settings);

        foreach($settings as $key => $value) {
            $this->offsetSet($key, $value);
        }
    }

    final public function get(string $key, $default = null) {
        return $this->offsetExists($key) ? $this->offsetGet($key) : $default;
    }

    final public function set(string $key, $value) : void {
        $this->offsetSet($key, $value);
    }

    final public function load() {
        acf_add_local_field_group((array) $this);
    }

}