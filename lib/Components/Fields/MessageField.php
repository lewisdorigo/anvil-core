<?php namespace Anvil\Components\Fields;

class MessageField extends AbstractField {

    protected function type() : string {
        return 'message';
    }

    protected function defaultSettings() : array {
        return [
            'message' => '',
            'esc_html' => false
        ];
    }

}