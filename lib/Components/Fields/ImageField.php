<?php namespace Anvil\Components\Fields;

class ImageField extends AbstractField {

    protected function type() : string {
        return 'image';
    }

    protected function defaultSettings() : array {
        return [
			'return_format' => 'array',
			'preview_size' => 'medium',
			'library' => 'all',
			'min_width' => '',
			'min_height' => '',
			'min_size' => '',
			'max_width' => '',
			'max_height' => '',
			'max_size' => '',
			'mime_types' => '',
        ];
    }

}