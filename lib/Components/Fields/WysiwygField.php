<?php namespace Anvil\Components\Fields;

class WysiwygField extends AbstractField {
    
    protected function type() : string {
        return 'wysiwyg';
    }
    
    protected function defaultSettings() : array {
        return [
            'default_value' => '',
            'tabs' => 'all',
            'toolbar' => 'basic',
            'media_upload' => 0,
            'delay' => 0
        ];
    }
    
}