<?php namespace Anvil\Components\Fields;

class DateTimeField extends AbstractField {

    protected function type() : string {
        return 'date_time_picker';
    }

    protected function defaultSettings() : array {
        return [
			'display_format' => 'j F Y g:ia',
			'return_format' => 'u',
			'first_day' => 1,
        ];
    }

}