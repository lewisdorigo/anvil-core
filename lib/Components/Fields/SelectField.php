<?php namespace Anvil\Components\Fields;

class SelectField extends AbstractField {
    
    protected function type() : string {
        return 'select';
    }
    
    protected function defaultSettings() : array {
        return [
            'choices' => [],
            'default_value' => '',
            'allow_null' => 0,
            'multiple' => 0,
            'ui' => 0,
            'return_format' => 1,
            'ajax' => 0,
            'placeholder' => ''
        ];
    }
    
}