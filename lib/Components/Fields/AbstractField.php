<?php namespace Anvil\Components\Fields;

use Anvil\Components\Fields\FieldsStore;

abstract class AbstractField extends \ArrayIterator {

    private $store;

    final public function __construct(string $title, array $settings = []) {

        $this->store = FieldsStore::getInstance();

        $this->setName($title);
        $this->setType();
        $this->setKey();

        $this->combineSettings($settings);

        $this->store->offsetSet($this->offsetGet('key'), $this);
    }

    abstract protected function type() : string;
    abstract protected function defaultSettings() : array;

    final private function setName(string $title) : void {

        $name = sanitize_title($title);

        $this->offsetSet('name', $name);
        $this->offsetSet('label', $title);
    }

    final private function setType() : void {
        $this->offsetSet('type', $this->type());
    }

    final private function setKey() : void {
        if($this->offsetExists('key')) {
            return;
        }

        $i = 0;

        $name = $this->offsetGet('name');
        $type = $this->offsetGet('type');

        $key = "field_anvil_{$type}-{$name}";

        while($this->store->exists($key)) {
            $i++;
            $key = "{$key}-{$i}";
        }

        $this->offsetSet('key', $key);
    }

    final public function combineSettings(array $settings) : void {

        $defaults = $this->defaultSettings();

        $settings = array_merge([
            'instructions' => '',
            'required' => 0,
            'conditional_logic' => 0,
            'wrapper' => [
                'width' =>'' ,
                'class' => '',
                'id' => '',
            ]
        ], $defaults, $settings);

        foreach($settings as $key => $value) {
            $this->offsetSet($key, $value);
        }
    }

}