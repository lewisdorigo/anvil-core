<?php namespace Anvil\Components\Fields;

class LinkField extends AbstractField {
    
    protected function type() : string {
        return 'link';
    }
    
    protected function defaultSettings() : array {
        return [
            'return_format' => 'array'
        ];
    }
    
}