<?php namespace Anvil\Components\Fields;

class UrlField extends AbstractField {

    protected function type() : string {
        return 'url';
    }

    protected function defaultSettings() : array {
        return [
            'default_value' => '',
            'placeholder' => '',
        ];
    }

}