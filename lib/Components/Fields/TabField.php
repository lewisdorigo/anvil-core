<?php namespace Anvil\Components\Fields;

class TabField extends AbstractField {

    protected function type() : string {
        return 'tab';
    }

    protected function defaultSettings() : array {
        return [
            'placement' => 'top',
            'endpoint' => false
        ];
    }

}