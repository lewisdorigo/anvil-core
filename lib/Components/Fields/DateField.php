<?php namespace Anvil\Components\Fields;

class DateField extends AbstractField {

    protected function type() : string {
        return 'date_picker';
    }

    protected function defaultSettings() : array {
        return [
			'display_format' => 'j F Y',
			'return_format' => 'U',
			'first_day' => 1,
        ];
    }

}