<?php namespace Anvil\Components;

use Anvil\Components\AbstractComponent;

abstract class AbstractMolecule extends AbstractComponent {
    
    final protected function type() : string {
        return 'molecule';
    }
    
}