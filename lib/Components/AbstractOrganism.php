<?php namespace Anvil\Components;

use Anvil\Components\AbstractComponent;

abstract class AbstractOrganism extends AbstractComponent {
    
    final protected function type() : string {
        return 'organism';
    }
    
}