<?php namespace Anvil\Components;

use Dorigo\Singleton\Singleton;
use Anvil\Components\ComponentStore;

class Components extends Singleton {

    private $store;
    private $directory = 'store';
    private $registry = null;
    private $loaded = [];

    protected function __construct() {
        $this->store = ComponentStore::getInstance();

        add_action('init', [$this, 'loadComponentRegistry'], 9999);
    }

    public function load() {

        foreach($this->store as $component) {
            $this->addComponent($component);
        }

    }

    public function loadComponentRegistry() {

        $blocks = \WP_Block_Type_Registry::get_instance()->get_all_registered();
        $this->registry = array_keys($blocks);

        set_transient('anvil-full-component-list', $this->registry);

    }

    public function getComponentRegistry() {
        if(is_null($this->registry)) {
            return get_transient('anvil-full-component-list', $this->registry);
        }

        return $this->registry;
    }


    protected function addComponent($component) {

        $slug = $component->getKey();

        if(in_array($slug, $this->loaded)) {
            throw new \Exception('Component with name '.$title.' already exists');
        }

        if($component->get('wp-block')) {
            $title = $component->getTitle();
            $settings = (array) $component->getSettings();

            $settings = array_merge([
                'name' => $slug,
                'title' => $title,
                'description' => '',
                'render_callback' => [$component, 'render'],
                'category' => 'common',
                'mode' => 'edit'
            ], $settings);

            if($component->isOverride()) {

                $this->handleOverride($component);
                //register_block_type($component->getOverride(), $settings);

            } else {

                acf_register_block($settings);

            }


            if($component->hasVariants() || $component->hasFields()) {
                acf_add_local_field_group($component->getFieldGroup());
            }
        }

        $this->loaded[] = $slug;

    }

    protected function handleOverride($component) {

        $slug = $component->getOverride();
        $settings = (array) $component->getSettings();

        $block = (array) \WP_Block_Type_Registry::get_instance()->get_registered($slug);

        if($block) {
            $settings = array_merge($block, [
                'render_callback' => [$component, 'render'],
            ], $settings);

            unregister_block_type($slug);
        }

        register_block_type($slug, $settings);

    }

}