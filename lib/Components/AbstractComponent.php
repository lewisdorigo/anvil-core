<?php namespace Anvil\Components;

use Anvil\Components\Settings;
use Anvil\Components\ComponentStore;
use Anvil\FileSystem\FileSystem;
use Anvil\Components\Fields\Group\FieldGroup;
use Anvil\Components\Fields\SelectField;
use Timber\Timber;

abstract class AbstractComponent {

    private $variants = [];
    private $settings;

    private $fields = [];
    private $fieldGroup;

    private $title;
    private $key;

    private $dataFilters = [];
    private $contentFitlers = [];

    private $override = false;

    abstract protected function __construct();
    abstract protected function type() : string;

    final public static function register() {

        $calledClass = get_called_class();
        $store = ComponentStore::getInstance();

        $component = new $calledClass();
        $store->add($component);

        return $component;
    }

    final public function addFields(array $fields) : void {

        foreach($fields as $field) {
            $this->addField($field);
        }

    }

    final public function getTitle() : string {

        if(!is_null($this->title)) {
            return $this->title;
        }


        $class = substr(strrchr(get_class($this), '\\'), 1);

        $this->title = ucwords(preg_replace('/([a-z0-9])([A-Z])/', '\1 \2', $class));
        return $this->title;

    }

    final public function getKey() : string {

        if(!is_null($this->key)) {
            return $this->key;
        }

        $this->key = sanitize_title($this->getTitle());
        return $this->key;
    }

    final public function hasFields() : bool {
        return !empty($this->fields);
    }

    final public function getFields() : array {
        return $this->fields;
    }

    final public function getFieldGroup() : array {

        if(is_null($this->fieldGroup)) {

            $fields = $this->getFields();

            if($variantFields = $this->getVariantFields()) {
                array_unshift($fields, $variantFields);
            }

            $this->fieldGroup = new FieldGroup($this->getTitle().' Fields', $fields);
            $this->fieldGroup->set('key', 'group_anvil_component_'.$this->getKey());
            $this->fieldGroup->set('location', [
                [
                    [
                        'param' => 'block',
                        'operator' => '==',
                        'value' => 'acf/'.$this->getKey(),
                    ]
                ]
            ]);

        }

        return (array) $this->fieldGroup;
    }

    final public function setOverride(string $key) : void {
        $this->override = $key;
    }

    final public function getOverride() : string {
        return $this->override;
    }

    final public function isOverride() : bool {
        return !!$this->override;
    }

    final public function getSettings() : Settings {
        if(is_null($this->settings)) {
            $this->settings = new Settings();
        }

        return $this->settings;
    }

    final public function addField($field) {
        $this->fields[] = $field;
    }

    final public function set(string $key, $value) {
        $settings = $this->getSettings();
        $settings->set($key, $value);
    }

    final public function unset(string $key) {

        $settings = $this->getSettings();
        $settings->unset($key, $value);

    }

    final public function get(string $key, $default = null) {

        $settings = $this->getSettings();
        return $settings->get($key, $default);

    }

    final public function addDataFilter($callback) {
        if(is_callable($callback)) {
            $this->dataFilters[] = $callback;
        }
    }

    final public function hasVariants() {
        return !empty($this->variants);
    }

    final public function addVariant(string $variant) {
        if(!in_array($variant, $this->variants)) {
            $this->variants[] = $variant;
        }
    }

    final public function getVariantFields() {
        if(!$this->hasVariants()) { return false; }

        $variants = [
            'index' => 'Default',
        ];

        foreach($this->variants as $variant) {
            $variants[$variant] = ucwords(str_replace(['-','_'], ' ', $variant));
        }

        return new SelectField('variant', [
            'required' => 1,
            'choices' => $variants
        ]);

    }

    final protected function runDataFilters(array $data, array $attributes = [], string $content = '') : array {
        foreach($this->dataFilters as $callback) {
            if(is_callable($callback)) {
                $data = call_user_func($callback, $data, $attributes, $content);
            }
        }

        return $data;
    }

    final protected function getTemplate() {
        return $this->get('template');
    }

    final public function getTemplatePath($variant = false) {
        $templates = [];

        if($template = $this->get('template')) {

            if($variant && $variant !== 'index' && $variant !== 'default') {
                $templates[] = "{$this->type()}s/{$template}/{$variant}.twig";
                $templates[] = "{$this->type()}s/{$template}/index-{$variant}.twig";
            }

            $templates[] = "{$this->type()}s/{$template}/index.twig";
        }

        if($variant && $variant !== 'index' && $variant !== 'default') {
            $templates[] = "{$this->type()}s/{$this->getKey()}/{$variant}.twig";
            $templates[] = "{$this->type()}s/{$this->getKey()}/index-{$variant}.twig";
        }

        $templates[] = "{$this->type()}s/{$this->getKey()}/index.twig";

        return $templates;
    }


    final protected function getDomDocument(string $content) {

        $dom = new \DOMDocument();
        $dom->loadHTML('<meta http-equiv="content-type" content="text/html; charset=utf-8">'.$content, LIBXML_HTML_NOIMPLIED | LIBXML_NOERROR);

        return $dom;

    }

    final protected function getTagContents(\DOMDocument $dom, string $tagName, string $exclude = null) {
        $elements = $dom->getElementsByTagName($tagName);

        $html = '';

        foreach($elements as $element) {

            foreach($element->childNodes as $node) {
                if($exclude && $node->nodeName === $exclude) { continue; }

                $html .= $dom->saveHTML($node);
            }

        }

        return $html;
    }

    protected function getTagAttributes(\DOMDocument $dom, string $tagName, array $allowed_attributes = []) {
        $elements = $dom->getElementsByTagName($tagName);

        $attributes = array_fill_keys($allowed_attributes, null);

        foreach($elements as $element) {

            foreach($element->attributes as $name => $data) {
                if($allowed_attributes && !in_array($name, $allowed_attributes)) { continue; }

                $attributes[$name] = $data->value;
            }
        }

        return $attributes;
    }

    protected function parseStyle(string $className = '') : string {
        preg_match('/is-style-([\w\-]+)/', $className, $style);

        return $style && isset($style[1]) ? $style[1] : '';

    }

    public function render(array $attributes = [], string $content = '') {

        $data = isset($attributes['data']) ? $attributes['data'] : [];
        $data['id'] = isset($attributes['id']) ? $attributes['id'] : null;
        $data['class'] = isset($attributes['className']) ? $attributes['className'] : null;
        $data['align'] = isset($attributes['align']) ? $attributes['align'] : null;

        $data['style'] = isset($attributes['className']) ? $this->parseStyle($attributes['className']) : null;

        $data = $this->runDataFilters($data, $attributes, $content);

        $variant = isset($data['variant']) ? $data['variant'] : null;

        $template = $this->getTemplatePath($variant);


        $context  = Timber::context();
        $data = array_merge($context, $data);

        if($this->isOverride()) {
            return Timber::fetch($template, $data);
        }

        return Timber::render($template, $data);
    }

}