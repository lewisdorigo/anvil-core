<?php namespace Anvil\Components;

use Anvil\Components\AbstractComponent;

abstract class AbstractAtom extends AbstractComponent {
    
    final protected function type() : string {
        return 'atom';
    }
    
}