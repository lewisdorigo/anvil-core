<?php namespace Anvil\Components;

use Anvil\Store\AbstractStore;

class ComponentStore extends AbstractStore {
    
    protected static $instance;
    protected $moduleLoaded = [];
    
    final protected function __construct() { }
    
    final public static function getInstance() {
        
        if(is_null(self::$instance)) {
            self::$instance = new self;
        }
        
        return self::$instance;
        
    }
}