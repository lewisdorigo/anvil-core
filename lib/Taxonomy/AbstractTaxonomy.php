<?php namespace Anvil\Taxonomy;

use Anvil\Taxonomy\TaxonomyStore;

abstract class AbstractTaxonomy {

    private $slug;
    private $options = [];
    private $object;

    private $baseLabel;
    private $label;
    private $plural;
    private $postTypes = [];

    private $registered = false;

    private static $defaultObject = [
        'public' => true,
        'publicly_queryable' => true,
        'labels' => [],
        'show_ui' => true,
        'show_in_menu' => true,
        'show_in_nav_menus' => false,
        'show_in_rest' => true,
        'show_tagcloud' => false,
        'show_in_quick_edit' => true,
        'show_admin_column' => true,
        'hierarchical' => false,
    ];

    private static $defaultLabels = [
        'name' => '{{ plural }}',
        'singular_name' => '{{ singular }}',
        'search_items' => 'Search {{ plural }}',
        'popular_items' => 'Popular {{ plural }}',
        'all_items' => 'All {{ plural }}',
        'parent_item' => 'Parent {{ singular }}',
        'parent_item_colon' => 'Parent {{ singular }}:',
        'edit_item' => 'Edit {{ singular }}',
        'view_item' => 'View {{ singular }}',
        'update_item' => 'Update {{ singular }}',
        'add_new_item' => 'Add New {{ singular }}',
        'new_item_name' => 'New {{ singular }} Name',
        'separate_items_with_commas' => 'Separate {{ plural }} with commas',
        'add_or_remove_items' => 'Add or remove {{ plural }}',
        'choose_from_most_used' => 'Choose from the most used {{ plural }}',
        'not_found' => 'No {{ plural }} found.',
        'no_terms' => 'No {{ plural }}',
        'items_list_navigation' => '{{ plural }} list navigation',
        'items_list' => '{{ plural }} list',
        'most_used' => 'Most Used',
        'back_to_items' => '← Back to {{ plural }}',
        'menu_name' => '{{ plural }}',
        'name_admin_bar' => '{{ plural }}',
        'archives' => 'All {{ plural }}',
    ];

    final protected function __construct() {
        $this->load();
        add_action('init', [$this, 'createTaxonomy']);
    }

    final public static function register() {

        $calledClass = get_called_class();
        $store = TaxonomyStore::getInstance();

        $component = new $calledClass();
        $store->add($component);

        return $component;

    }

    abstract protected function load() : void;

    final protected function set(string $key, $value = null) : void {
        $this->options[$key] = $value;
    }

    final protected function get(string $key, $default = null) {
        return isset($this->options[$key]) ? $this->options[$key] : $value;
    }

    public function getSlug() : string {
        if(is_null($this->slug)) {

            $label = $this->getBaseLabel();
            $this->slug = sanitize_title($label);

        }

        return $this->slug;
    }

    public function getBaseLabel() {

        if(is_null($this->baseLabel)) {

            $class = substr(strrchr(get_class($this), '\\'), 1);
            $this->baseLabel = preg_replace('/([a-z0-9])([A-Z])/', '\1 \2', $class);

        }

        return $this->baseLabel;
    }

    public function getLabel() : string {

        if(is_null($this->label)) {

            $this->label = $this->getBaseLabel();

        }

        return $this->label;
    }

    public function addPostType(string $postType) : void {
        if(!in_array($postType, $this->postTypes)) {
            $this->postTypes[] = $postType;
        }
    }

    public function removePostType(string $postType) : void {
        array_diff($this->postTypes, [$postType]);
    }

    public function createTaxonomy() : void {

        if($this->registered) { return; }

        $slug = $this->getSlug();

        $object = array_merge(self::$defaultObject, $this->options);
        $object['labels'] = array_merge($this->generateLabels(), $object['labels']);

        register_taxonomy($slug, $this->postTypes, $object);

        $this->registered = true;

    }

    private function generateLabels() : array {
        $singular = $this->getLabel();
        $plural   = !is_null($this->plural) ? $this->plural : $singular.'s';

        return array_map(function($label) use($singular, $plural) {
            return str_replace(['{{ singular }}','{{ plural }}'], [$singular, $plural], $label);
        }, self::$defaultLabels);
    }

    public function setSingular(string $singular) : void {
        $this->label = $singular;
    }

    public function setPlural(string $plural) : void {
        $this->plural = $plural;
    }

    final public function addFieldGroup($group) {

        $location = $group->get('location', []);

        $location[] = [
            [
                'param' => 'taxonomy',
                'operator' => '==',
                'value' => $this->getSlug(),
            ],
        ];

        $group->set('location', $location);

        $group->load();
    }
}