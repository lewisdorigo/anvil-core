<?php namespace Anvil;

use Anvil\Components\Components;
use Anvil\FileSystem\FileSystem;
use Anvil\PostType\PostTypes;
use Anvil\Twig\Extension as TwigExtension;
use Dorigo\Singleton\Singleton;
use Timber\{Timber,PostQuery,Post};

class Anvil extends Singleton {
    protected $templates = [];
    protected $templateType;

    protected $templateDirectories;

    protected function __construct() {

        add_filter('index_template',      [$this, 'queryTemplate'], 109, 3);
        add_filter('404_template',        [$this, 'queryTemplate'], 109, 3);
        add_filter('archive_template',    [$this, 'queryTemplate'], 109, 3);
        add_filter('author_template',     [$this, 'queryTemplate'], 109, 3);
        add_filter('category_template',   [$this, 'queryTemplate'], 109, 3);
        add_filter('tag_template',        [$this, 'queryTemplate'], 109, 3);
        add_filter('taxonomy_template',   [$this, 'queryTemplate'], 109, 3);
        add_filter('date_template',       [$this, 'queryTemplate'], 109, 3);
        add_filter('embed_template',      [$this, 'queryTemplate'], 109, 3);
        add_filter('home_template',       [$this, 'queryTemplate'], 109, 3);
        add_filter('frontpage_template',  [$this, 'queryTemplate'], 109, 3);
        add_filter('page_template',       [$this, 'queryTemplate'], 109, 3);
        add_filter('paged_template',      [$this, 'queryTemplate'], 109, 3);
        add_filter('search_template',     [$this, 'queryTemplate'], 109, 3);
        add_filter('single_template',     [$this, 'queryTemplate'], 109, 3);
        add_filter('singular_template',   [$this, 'queryTemplate'], 109, 3);
        add_filter('attachment_template', [$this, 'queryTemplate'], 109, 3);

        add_action('init', [$this,'setTimberLocations']);
        add_action('init', [$this,'loadComponents']);

        add_filter('wp_prepare_themes_for_js', [$this, 'removeBaseTheme']);

        $this->loadScripts();

        $this->loadTwigExtensions();

        $this->addPostTypes();

    }

    final public function loadTwigExtensions() {

        TwigExtension::getInstance();

    }

    final public function loadComponents() {
        $components = Components::getInstance();
        $components->load();
    }

    final public function addPostTypes() {

        $postTypes = PostTypes::getInstance();

    }

    final public function removeBaseTheme($themes) {

        unset($themes['anvil-core']);
        return $themes;

    }

    final public function setTimberLocations() {

        Timber::$locations = FileSystem::getViewDirectories();

    }

    public function queryTemplate($template, $type, $templates) {
        $this->templateType = $type;

        $this->templates = array_merge($this->templates, $templates);
        $this->templates = array_unique($this->templates);

        return $template;
    }

    public function getTemplates() : array {
        return apply_filters('Anvil\Templates', $this->templates);
    }

    final public function getTemplate() : array {

        $templates = array_map(function($file) {
            return preg_replace('/\.php$/', '.twig', $file);
        }, $this->getTemplates());

        $templates[] = 'index.twig';
        return array_unique($templates);

    }

    final protected function loadScripts() {
        FileSystem::loadScripts();
    }

    final public function render() {
        Timber::$context_cache = [];
        $context  = Timber::context();
        $template = $this->getTemplate();

        $post = Timber::query_post();

        $context['post'] = $post;

        if((is_singular() || is_single() || is_page()) && post_password_required($post->ID)) {

            if(!defined('DONOTCACHEPAGE')) {
                define('DONOTCACHEPAGE', true);
            }

            array_unshift($template, 'single-password.twig');
        }

        $accept = isset($_SERVER['HTTP_ACCEPT']) ? $_SERVER['HTTP_ACCEPT'] : 'text/html';

        if(preg_match('/application\/json/', $accept)) {

            if(!defined('DONOTCACHEPAGE')) {
                define('DONOTCACHEPAGE', true);
            }

            header('Content-Type: application/json');
            echo json_encode($context, JSON_PRETTY_PRINT);
            exit;
        }

        Timber::render($template, $context);
        exit;
    }
}