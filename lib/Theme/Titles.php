<?php namespace Anvil\Theme;

class AnvilTitle extends \ArrayIterator {

    public function set(string $type, $content = null) {
        $this->offsetSet($type, $content);
    }

    public function get(string $type, $default = null) {
        return $this->offsetExists($type) ? $this->offsetGet($type) : $default;
    }

    public function __set(string $key, $value = null) {
        $this->set($key, $value);
    }

    public function __get(string $key) {
        return $this->get($key);
    }

}

class Titles {
    public function __construct($post = null) {
        global $wp_query;

        $this->object = $post ?: get_queried_object();
        $this->object = !is_object($this->object) ? get_post($post) : $this->object;

        if(!$post) {
            if($wp_query->is_404()) {

                return $this->notFound();

            } else if($wp_query->is_search()) {

                return $this->search();

            } else if ($wp_query->is_date()) {

                return $this->date();

            }
        }

        switch(get_class($this->object)) {
            case 'WP_Post':
                $this->post();
                break;
            case 'WP_Term':
                $this->taxonomy();
                break;
            case 'WP_Post_Type':
                $this->postType();
                break;
            case 'WP_User':
                $this->user();
                break;
        }

    }

    private function post() {

        $titles = new AnvilTitle();

        $titles->title   = get_field('head_override_content', $this->object->ID) ? get_field('head_title', $this->object->ID) : get_the_title($this->object->ID);
        $titles->content = get_field('head_override_content', $this->object->ID) ? get_field('head_content', $this->object->ID) : null;
        $titles->image   = get_field('head_image', $this->object->ID, 'array') ?: null;

        $titles = apply_filters('Anvil\Titles', $titles, $this->object);
        $titles = apply_filters('Anvil\Titles\Post', $titles, $this->object);

        $this->titles = apply_filters('Anvil\Titles\Post\post_type='.$this->object->post_type, $titles, $this->object);

        unset($titles);

    }

    private function taxonomy() {

        $titles = new AnvilTitle();

        $titles->title   = get_field('head_override_content', $this->object) ? get_field('head_title', $this->object) : $this->object->name;
        $titles->content = get_field('head_override_content', $this->object) ? get_field('head_content', $this->object) : wpautop($this->object->description);
        $titles->image   = get_field('head_image', $this->object, 'array') ?: null;


        $titles = apply_filters('Anvil\Titles', $titles, $this->object);
        $this->titles = apply_filters('Anvil\Titles\Taxonomy', $titles, $this->object);

        unset($titles);
    }

    private function postType() {

        $type = $this->object->name;
        $labels = $this->object->labels;

        $titles = new AnvilTitle();

        if($archive = get_field("page_for_{$type}",'options')) {

            $titles = new self($archive);
            $titles = $titles->get();

        } else {

            $titles->title   = get_field("head_override_content_{$type}", 'options') ? get_field("head_title_{$type}",   'options') : $labels->name;
            $titles->content = get_field("head_override_content_{$type}", 'options') ? get_field("head_content_{$type}", 'options') : null;
            $titles->image   = get_field("head_image_{$type}", 'options', 'array') ?: null;

        }

        $titles = apply_filters('Anvil\Titles', $titles, $this->object);
        $titles = apply_filters('Anvil\Titles\PostType', $titles, $this->object);

        $this->titles = apply_filters("Anvil\Titles\post_type={$type}", $titles, $this->object);

        unset($titles);

    }

    private function user() {

        $user = $this->object;

        $titles = new AnvilTitle();
        $titles->title = "Posts by {$user->display_name}";

        $titles = apply_filters('Anvil\Titles', $titles, $user);
        $this->titles = apply_filters('Anvil\Titles\User', $titles, $user);

        unset($titles);
    }

    private function notFound() {

        $titles = new AnvilTitle();

        $titles->title   = 'Page not found';
        $titles->content = null;
        $titles->image   = null;

        $titles = apply_filters('Anvil\Titles', $titles, $this->object);
        $this->titles = apply_filters('Anvil\Titles\NotFound', $titles, $this->object);

        unset($titles);

    }

    private function search() {

        $titles = new AnvilTitle();

        $titles->title   = 'You searched for “'.get_search_query().'”';
        $titles->content = null;
        $titles->image   = null;

        $titles = apply_filters('Anvil\Titles', $titles, $this->object);
        $this->titles = apply_filters('Anvil\Titles\Search', $titles, $this->object);

        unset($titles);

    }

    private function date() {
        if($posts_page = get_option('page_for_posts')) {
            $object = get_post($posts_page);
        } else {
            $object = get_post_type_object('post');
        }

        $titles = new self($object);
        $this->titles = apply_filters('Anvil\Titles\Date', $titles->get(), null);

        unset($titles);
    }

    public function get($type = null) {
        if(is_a($this->titles, '\\Anvil\\Theme\\AnvilTitle')) {

            return !is_null($type) ? $this->titles->get($type) : $this->titles;

        } else {

            return null;

        }
    }
}