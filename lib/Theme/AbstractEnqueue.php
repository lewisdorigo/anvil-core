<?php namespace Anvil\Theme;

use Anvil\Theme\ResourceIntegrity;
use Anvil\Theme\File;
use Anvil\Theme\{StyleStore, ScriptStore};

abstract class AbstractEnqueue {

    protected $styles;
    protected $scripts;

    protected $stylesLoaded = false;
    protected $scriptsLoaded = false;

    protected static $resourceIntegrityLoaded;

    public function __construct() {
        $this->styles = new StyleStore;
        $this->scripts = new ScriptStore;

        $this->load();

        add_action('wp_enqueue_scripts', [$this,'loadStyles']);
        add_action('wp_enqueue_scripts', [$this,'loadScripts']);

        if(!self::$resourceIntegrityLoaded) {

            add_filter('script_loader_tag', [$this, 'resourceIntegrityTag'], 10, 3);
            self::$resourceIntegrityLoaded = true;

        }
    }

    abstract protected function load() : void;

    final protected function addStyle(string $key, string $source, array $dependancies = [], bool $external = false, string $media = null) : void {

        $this->styles->set($key, [
            'source' => $source,
            'dependencies' => $dependancies,
            'external' => $external,
            'media' => $media
        ]);
    }

    final protected function addStyles(array $styles) : void  {

        foreach($styles as $key => $data) {

            $this->addStyle($key, ...$data);

        }

    }

    final protected function addScript(string $key, string $source, array $dependancies = [], bool $external = false, string $footer = null) : void {

        $this->scripts->set($key, [
            'source' => $source,
            'dependencies' => $dependancies,
            'external' => $external,
            'footer' => $footer
        ]);

    }

    final protected function addScripts(array $scripts) : void  {

        foreach($scripts as $key => $data) {

            $this->addScript($key, ...$data);

        }

    }

    final public function loadStyles() : void {

        if($this->stylesLoaded) { return; }

        foreach($this->styles as $key => $data) {
            $source       = isset($data['source']) ? $data['source'] : null;
            $dependancies = isset($data['dependencies']) ? $data['dependencies'] : [];

            $external = isset($data['external']) ? $data['external'] : true;
            $media    = isset($data['media']) ? $data['media'] : null;

            if(!$source) { continue; }

            $file = !$external ? File::get($source, false, true) : $source;

            if($file) {
                wp_enqueue_style($key, $file, $dependancies, null, $media);
            }
        }

        $this->stylesLoaded = true;

    }

    final public function loadScripts() : void  {

        if($this->scriptsLoaded) { return; }

        foreach($this->scripts as $key => $data) {
            $source       = isset($data['source']) ? $data['source'] : null;
            $dependancies = isset($data['dependencies']) ? $data['dependencies'] : [];

            $external = isset($data['external']) ? $data['external'] : true;
            $footer   = isset($data['footer']) ? $data['footer'] : true;

            if(!$source) { continue; }

            $file = !$external ? File::get($source, false, true) : $source;

            if($file) {
                wp_enqueue_script($key, $file, $dependancies, null, $footer);
            }
        }

        $this->scriptsLoaded = true;

    }

    public function resourceIntegrityTag($tag, $handle, $source) {

        if(is_admin()) { return $tag; }

        $integrity = new ResourceIntegrity($source);

        if((string) $integrity) {
            $tag = str_replace("src='", "integrity='{$integrity}' crossorigin='anonymous' src='", $tag);
        }

        return $tag;

    }

}