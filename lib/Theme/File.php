<?php namespace Anvil\Theme;

class File {

    public static function get(string $file, bool $directory = false, bool $cacheBusting = false) {
        $file = ltrim($file, DIRECTORY_SEPARATOR);

        $path = locate_template($file);

        if(!$path) { return false; }

        if(!$directory && $cacheBusting) {
            $ext = pathinfo($path, PATHINFO_EXTENSION);
            $modified = filemtime($path);

            if(defined('WP_ENV') && WP_ENV === 'development') {
                $path = "{$path}?m={$modified}";
            } else {
                $path = str_replace(".{$ext}", ".{$modified}.{$ext}", $path);
            }
        }

        return $directory ? $path : str_replace(WP_WEBROOT, WP_SITEURL, $path);
    }

}