<?php namespace Anvil\Theme;

class ResourceIntegrity {

    private $key;
    private $id;

    private $source;
    private $sourceHost;
    private $host;

    private $digest;
    private $algorithm;

    private $enabled = false;

    public function __construct($source, $algorithm = 'sha256') {
        $this->source = $source;
        $this->algorithm = $algorithm;

        $this->host = parse_url(WP_HOME, PHP_URL_HOST);
        $this->sourceHost = parse_url($source, PHP_URL_HOST);

        $this->enabled = $this->sourceHost && $this->host !== $this->sourceHost;

        $this->id = hash('md5', $source.$algorithm);
        $this->key = "anvil-integrity-digest-{$this->id}";

        $this->getDigest();
    }

    public function getDigest() {

        if(!$this->enabled) { return ''; }

        if(is_null($this->digest)) {

            if($digest = get_site_transient($this->key)) {

                $this->digest = $digest;

            } else {

                $this->digest = $this->generateDigest();
                set_site_transient($this->key, $this->digest, WEEK_IN_SECONDS);

            }
        }

        return $this->digest;
    }

    public function generateDigest() : string {

        $curl = curl_init();
        $headers = [];
        $digest = '';

        curl_setopt($curl, CURLOPT_URL, $this->source);
        curl_setopt($curl, CURLOPT_BINARYTRANSFER, true);
        curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($curl, CURLOPT_HEADER, 1);

        curl_setopt($curl, CURLOPT_HEADERFUNCTION, function($curl, $header) use(&$headers) {
            $len = strlen($header);
            $header = explode(':', $header, 2);

            if (count($header) < 2) { return $len; }

            $name = strtolower(trim($header[0]));

            if (!array_key_exists($name, $headers)) {
                $headers[$name] = [trim($header[1])];
            } else {
                $headers[$name][] = trim($header[1]);
            }

            return $len;
        });

        $response = curl_exec($curl);
        $httpcode = curl_getinfo($curl, CURLINFO_HTTP_CODE);

        if($httpcode === 200 && array_key_exists('access-control-allow-origin', $headers)) {
            $body = explode("\r\n\r\n", $response, 2)[1];

            $digest = $this->generateHash($body);
        }

        return $digest;

    }

    private function generateHash(string $contents) {
        if($contents !== '') {
            $hash = hash($this->algorithm, $contents, true);

            return $this->algorithm.'-'.base64_encode($hash);
        }

        return '';
    }

    public function __toString() {
        return (string) $this->digest;
    }

}