<?php namespace Anvil\PostType;

use Dorigo\Singleton\Singleton;
use Anvil\PostType\Fields;
use Anvil\PostType\PostTypeStore;

class PostTypes extends Singleton {
    protected $store;

    protected function __construct() {
        $this->store = PostTypeStore::getInstance();

        Fields::getInstance();

        add_filter('display_post_states', [$this,'postStates'], 100, 2);
        add_filter('page_attributes_dropdown_pages_args', [$this,'removeFromParent'], 100, 2);
        add_filter('wp_nav_menu_objects', [$this,'navActiveStates'], 10, 2);

        add_action('pre_get_posts', [$this,'postTypeArchive'], 10, 1);
        add_action('template_redirect', [$this,'setPostPageContent'], 10, 1);

        add_filter('wpseo_title', [$this, 'seoTitle'], 100, 2);
        add_filter('wpseo_twitter_title', [$this, 'seoTitle'], 100, 2);
        add_filter('wpseo_opengraph_title', [$this, 'seoTitle'], 100, 2);
    }

    public function isArchive($post_id = null) {

        $post = get_post($post_id);

        if(!$post) { return false; }

        if(!is_admin() && is_post_type_archive()) {
            return $post->post_type;
        }

        foreach($this->store as $postType) {
            $slug = $postType->getSlug();

            if($post->ID == get_field("page_for_{$slug}",'options')) {
                return $slug;
            }
        }

        return false;
    }

    public function postStates(array $postStates, \WP_Post $post) {
        if($archive = $this->isArchive($post->ID)) {
            $type = get_post_type_object($archive);
            $labels = get_post_type_labels($type);

            $postStates["{$archive}_archive"] = $labels->archives;
        }

        return $postStates;
    }

    public function removeFromParent(array $dropdown, $post) {
        $exclude = $dropdown["exclude_tree"];
        $exclude = !is_array($exclude) ? explode(",", $exclude) : $exclude;

        foreach($this->store as $postType) {
            $slug = $postType->getSlug();
            if($archive = get_field("page_for_{$slug}",'options')) {
                $exclude[] = $archive;
            }
        }

        $dropdown["exclude_tree"];
        return $dropdown;
    }

    public static function navActiveStates($items, $args) {
        global $wp_query;

        $postType = null;
        $archive = null;

        if($wp_query->is_post_type_archive() || $wp_query->is_singular()) {
            $postType = $wp_query->get("post_type");
        }

        if(!$postType) { return $items; }

        $postType = $wp_query->get("post_type");
        $archive = get_field("page_for_{$postType}",'options');

        if(!$archive) { return $items; }

        $ancestors = get_post_ancestors($archive);
        $ancestors[] = $archive;

        array_walk($items, function(&$item, $id) use($ancestors) {
            if($item->type !== "post_type") { return true; }

            if(in_array((int) $item->object_id, $ancestors)) {
                $item->classes[] = 'current-menu-ancestor';
            }
        });

        return $items;
    }

    public function postTypeArchive($query) {

        if(!$query->is_main_query() || (!$query->get('pagename') && !$query->get('page_id'))) {
            return $query;
        }

        if($query->get('pagename')) {
            $page = get_page_by_path($query->get('pagename'));
        } else {
            $page = get_post($query->get('page_id'));
        }

        if(!$page) { return $query; }

        $archive = null;

        foreach($this->store as $postType) {
            $slug = $postType->getSlug();

            if($page->ID == get_field("page_for_{$slug}", 'options')) {
                $archive = $slug;
                break;
            }
        }

        if(!$archive) { return $query; }
        $query->query = [
            'post_type' => $archive,
        ];

        $query->set('pagename', '');
        $query->set('post_type', $archive);

        $query->is_page = false;
        $query->is_singular = false;
        $query->is_post_type_archive = true;
        $query->is_custom_archive = true;
        $query->queried_object = get_post_type_object($archive);

        return $query;
    }

    public function setPostPageContent() {
        global $wp_query;

        if(isset($wp_query->is_custom_archive)) {

            $postType = $wp_query->get('post_type');

            if($archive = get_field("page_for_{$postType}", 'options')) {
                $post = get_post($archive);
                $post->post_type = $postType;

                //$wp_query->post = $post;
            }

            $wp_query->reset_postdata();
        }
    }

    public function seoTitle($title) {
        $action = current_action();

        remove_filter($action, [$this, 'seoTitle'], 100, 2);

        if(is_post_type_archive()) {
            $object = get_queried_object();
            $archive = get_field("page_for_{$object->name}",'options');

            if(!$archive) { return $title; }

            $archive = get_post($archive);
            $meta = \YoastSEO()->meta->for_post($archive->ID); //->title;

            switch($action) {
                case 'wpseo_opengraph_title':
                    $title = $meta->open_graph_title;
                    break;
                case 'wpseo_twitter_title':
                    $title = $meta->twitter_title;
                    break;
                default:
                    $title = $meta->title;
                    break;
            }
        }

        add_filter($action, [$this, 'seoTitle'], 100, 2);

        return $title;
    }

}