<?php namespace Anvil\PostType;

use Anvil\PostType\PostTypeStore;

abstract class AbstractPostType {

    private $slug;
    private $options = [];
    private $object;

    private $baseLabel;
    private $label;
    private $plural;

    private static $defaultObject = [
        'public' => true,
        'labels' => [],
        'publicly_queryable' => true,
        'show_ui' => true,
        'exclude_from_search' => true,
        'show_in_nav_menus' => true,
        'show_in_admin_bar' => true,
        'has_archive' => true,
        'hierarchical' => false,
        'capability_type' => 'post',
        'show_in_rest' => true,
    ];

    private static $defaultLabels = [
        'name'                     => '{{ plural }}',
        'singular_name'            => '{{ singular }}',

        'add_new'                  => 'Add New',
        'add_new_item'             => 'Add a New {{ singular }}',

        'edit_item'                => 'Edit {{ singular }}',
        'new_item'                 => 'New {{ singular }}',

        'view_item'                => 'View {{ singular }}',
        'view_items'               => 'View {{ plural }}',
        'search_items'             => 'Search {{ plural }}',
        'not_found'                => 'No {{ plural }} found.',
        'not_found_in_trash'       => 'No {{ plural }} found in Bin.',

        'parent_item_colon'        => '',

        'all_items'                => 'All {{ plural }}',
        'archives'                 => '{{ singular }} Archives',

        'attributes'               => '{{ singular }} Attributes',

        'insert_into_item'         => 'Insert into {{ singular }}',
        'uploaded_to_this_item'    => 'Uploaded to this {{ singular }}',
        'featured_image'           => 'Featured Image',
        'set_featured_image'       => 'Set featured image',
        'remove_featured_image'    => 'Remove featured image',
        'use_featured_image'       => 'Use as featured image',

        'filter_items_list'        => 'Filter {{ plural }}',
        'items_list_navigation'    => '{{ singular }} list navigation',
        'items_list'               => '{{ singular }} list',

        'item_published'           => '{{ singular }} published.',
        'item_published_privately' => '{{ singular }} published privately.',
        'item_reverted_to_draft'   => '{{ singular }} reverted to draft.',
        'item_scheduled'           => '{{ singular }} scheduled.',
        'item_updated'             => '{{ singular }} updated.',

        'menu_name'                => '{{ plural }}',
        'name_admin_bar'           => '{{ singular }}'
    ];

    private $archive;
    private $registered = false;

    final protected function __construct() {
        $this->load();

        add_action('init', [$this, 'createPostType']);
    }

    final public static function register() {

        $calledClass = get_called_class();
        $store = PostTypeStore::getInstance();

        $component = new $calledClass();
        $store->add($component);

        return $component;

    }

    abstract protected function load() : void;

    final protected function set(string $key, $value = null) : void {
        $this->options[$key] = $value;
    }

    final public function get(string $key, $default = null) {
        return isset($this->options[$key]) ? $this->options[$key] : $value;
    }

    public function getBaseLabel() {

        if(is_null($this->baseLabel)) {

            $class = substr(strrchr(get_class($this), '\\'), 1);
            $this->baseLabel = preg_replace('/([a-z0-9])([A-Z])/', '\1 \2', $class);
        }

        return $this->baseLabel;

    }

    public function getSlug() : string {
        if(is_null($this->slug)) {

            $label = $this->getBaseLabel();
            $this->slug = sanitize_title($label);

        }

        return $this->slug;
    }

    final public function savePost($function, int $priority = 10, int $args = 1) {
        add_action('save_post_'.$this->getSlug(), $function, $priority, $args);
    }



    public function getLabel() : string {

        if(is_null($this->label)) {

            $this->label = $this->getBaseLabel();

        }

        return $this->label;
    }

    public function createPostType() : void {

        if($this->registered) { return; }

        $object = array_merge(self::$defaultObject, $this->options);
        $object['labels'] = array_merge($this->generateLabels(), $object['labels']);

        if($object['has_archive']) {

            $archive = $this->getArchive();
            $slug = $archive ? get_page_uri($archive) : $this->getSlug();

            $object['rewrite'] = [
                'slug' => $slug,
                'with_front' => false,
                'feeds' => false
            ];

        }

        $slug = $this->getSlug();

        register_post_type($slug, $object);

        $this->registered = true;

    }

    private function generateLabels() : array {
        $singular = $this->getLabel();
        $plural   = !is_null($this->plural) ? $this->plural : $singular.'s';

        return array_map(function($label) use($singular, $plural) {
            return str_replace(['{{ singular }}','{{ plural }}'], [$singular, $plural], $label);
        }, self::$defaultLabels);
    }

    public function setSingular(string $singular) : void {
        $this->label = $singular;
    }

    public function setPlural(string $plural) : void {
        $this->plural = $plural;
    }

    public function getObject() {

        if($this->registered) {
            return get_post_type_object($this->getSlug());
        }

        trigger_error('The post type is not registered yet.');
        exit;
    }

    public function getLabels() {

        $object = $this->getObject();
        return get_post_type_labels($object);

    }

    public function getArchive() {

        if(is_null($this->archive)) {
            $slug = $this->getSlug();
            $this->archive = get_field("page_for_{$slug}", 'options');
        }

        return $this->archive;

    }

    final public function addFieldGroup($group) {

        $location = $group->get('location', []);

        $location[] = [
            [
                'param' => 'post_type',
                'operator' => '==',
                'value' => $this->getSlug(),
            ],
        ];

        $group->set('location', $location);

        $group->load();
    }

}
