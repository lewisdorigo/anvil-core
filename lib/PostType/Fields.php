<?php namespace Anvil\PostType;

use Dorigo\Singleton\Singleton;
use Anvil\PostType\PostTypeStore;
use Anvil\Components\Fields\Group\FieldGroup;
use Anvil\Components\Fields\{MessageField, TabField, PostObjectField, WysiwygField, TextField};

class Fields extends Singleton  {

    final protected function __construct() {
        add_action('init', [$this, 'addArchives']);
        add_action('acf/save_post', [$this, 'saveFields']);
    }

    public function addArchives() {

        $types = PostTypeStore::getInstance();

        foreach($types as $type) {
            $labels = $type->getLabels();
            $slug = $type->getSlug();

            acf_add_options_sub_page(array(
                'page_title'     => "{$labels->singular_name} Options",
                'menu_title'     => 'Options',
                'parent'         => "edit.php?post_type={$slug}",
                'capability'	 => 'switch_themes',
                'slug'           => "anvil-post-type-archive-{$slug}"
            ));

            $typeFields = [
                new TextField("post-type-update-{$slug}", [
                    'wrapper' => [
                        'class' => 'hidden'
                    ]
                ]),
                new TabField("post_type_tab_{$slug}", [
                    'label' => $labels->name
                ]),
            ];

            if($type->get('public') && $type->get('has_archive')) {
                $typeFields[] = new PostObjectField("page_for_{$slug}", [
                    'label' => $labels->archives,
                    'post_type' => [
                        'page'
                    ],
                    'instructions' => 'What page do you want to use for the as the '.strtolower($labels->archives).'?',
                    'return_format' => 'id',
                ]);
            }

            $typeFields[] = new WysiwygField("no_{$slug}_found", [
                'label' => "No {$labels->name}",
                'instructions' => 'This text will be displayed when there are no '.strtolower($labels->name).'.',
                'media_upload' => false,
                'default_value' => $labels->not_found
            ]);

            $typeFields = apply_filters("Anvil/PostTypes/Options/post_type={$slug}", $typeFields);
            $typeFields = apply_filters("Anvil/PostTypes/Options", $typeFields);

            $group = new FieldGroup('Post Type Pages', $typeFields, [
                'location' => [
                    [
                        [
                            'param' => 'options_page',
                            'operator' => '==',
                            'value' => "anvil-post-type-archive-{$slug}",
                        ],
                    ],
                ],
                'style' => 'seamless',
                'label_placement' => 'left',
                'instruction_placement' => 'field',
            ]);

            $group->load();

        }

    }

    public function saveFields($post_id) {
        if($post_id !== "options") { return; }

        $post_data = isset($_POST['acf']) ? $_POST['acf'] : [];
        $keys = array_keys($post_data);

        $keys = array_reduce($keys, function(array $return, string $item) {
            if(preg_match('/^field_anvil_text-post-type-update-/i', $item, $matches)) {
                $return[] = $item;
            }

            return $return;
        }, []);

        $keys = array_filter($keys);

        if(!empty($keys)) {
            flush_rewrite_rules(false);
            return;
        }
    }

}