<?php namespace Anvil\FileSystem;

class FileSystem {

    private static $themeDirectories = [];
    private static $viewDirectories = [];
    private static $scriptDirectories = [];
    private static $includeHeirarchy = [];

    public static function path(...$parts) {
        return implode(DS, $parts);
    }

    public static function file($relativePaths, $load = false, $once = true) {

        $location = null;
        $paths = self::getIncludeHeirarchy();

        foreach((array) $relativePaths as $relativePath) {
            foreach($paths as $path) {
                $file = self::path($path, $relativePath);

                if(file_exists($file)) {
                    $location = $file;
                    break;
                }
            }
        }

        if($location && $load) {
            $once ? require_once $location : require $location;
        }

        return $location;
    }

    protected static function getIncludeHeirarchy() {
        if(!empty(self::$includeHeirarchy)) {
            return self::$includeHeirarchy;
        }

        self::$includeHeirarchy = self::getThemeDirectories();
        self::$includeHeirarchy[] = self::path(ANVIL_SOURCE);

        return self::$includeHeirarchy;
    }

    public static function getScriptDirectories() {
        if(!empty(self::$scriptDirectories)) {
            return self::$scriptDirectories;
        }

        self::$scriptDirectories = array_map(function($path) {
            return self::path($path, 'scripts');
        }, self::getIncludeHeirarchy());

        return self::$scriptDirectories;
    }

    public static function loadScripts() {
        $paths = self::getScriptDirectories();

        foreach($paths as $path) {

            self::requireDirectory($path);

        }
    }

    public static function requireDirectory(string $directory, bool $recursive = true) {

        if(!file_exists($directory) || !is_dir($directory)) { return false; }

        foreach(scandir($directory) as $file) {
            $path = self::path($directory, $file);
            $ext  = pathinfo($path, PATHINFO_EXTENSION);

            if(strpos($file, '.') === 0) {
                continue;
            } elseif(is_file($path) && $ext === "php") {
                require_once $path;
            } elseif(is_dir($path) && $recursive) {
                self::requireDirectory($path, true);
            }
        }

    }

    public static function getThemeDirectories() {
        if(!empty(self::$themeDirectories)) {
            return self::$themeDirectories;
        }

        $theme = wp_get_theme();

        $directories = [];

        if($name = $theme->get_stylesheet()) {
            $directories[] = self::path(ANVIL_THEMES, $name);
        }

        if($parent = $theme->get_template()) {
            $directories[] = self::path(ANVIL_THEMES, $parent);
        }

        $directories[] = self::path(ANVIL_THEMES, 'anvil-core');

        self::$themeDirectories = array_unique($directories);

        return self::$themeDirectories;
    }

    public static function getViewDirectories() {
        if(!empty(self::$viewDirectories)) {
            return self::$viewDirectories;
        }

        $themes = self::getThemeDirectories();
        self::$viewDirectories = array_map(function($theme) {
            return self::path($theme,'views');
        }, $themes);

        return self::$viewDirectories;

    }

}